from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('', views.page, name='page'),
    path('data/<str:searchKey>', views.data, name='data'),
]
