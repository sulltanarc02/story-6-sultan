from django.test import TestCase

class Test_Books(TestCase):
    def test_url(self):
        response = self.client.get("/books/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books/books.html')

    def test_searchBox(self):
        response = self.client.get("/books/data/sultan")
        self.assertEqual(response.status_code, 200)
