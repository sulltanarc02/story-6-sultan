from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def page(request):
    return render(request, 'books/books.html')

def data(request,*args, **kwargs):
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + kwargs["searchKey"]
    r = requests.get(url_tujuan)

    buku = r.json()

    data_hasil = {
        "hasilBuku": [],
    }

    buku = buku["items"]


    for tiapBuku in buku:
        bukuKei = tiapBuku["volumeInfo"]
        bukuJson = {}

        bukuJson["foto"] = bukuKei["imageLinks"]["thumbnail"] if "imageLinks" in bukuKei.keys() else "https://image.freepik.com/free-photo/vertical-view-eiffel-tower-paris-france_1258-3169.jpg"
        bukuJson["judul"] = bukuKei["title"] 
        bukuJson["deskripsi"] = bukuKei["description"] if "description" in bukuKei.keys() else " "

        data_hasil["hasilBuku"].append(bukuJson)

    return JsonResponse(data_hasil, safe=False)