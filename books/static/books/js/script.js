$("#keyword").keyup(function() {
    var isi_keyword = $("#keyword").val();
    var url_untuk_dipanggil = '/books/data/' + isi_keyword;
    console.log(isi_keyword);

    $.ajax({
        url: url_untuk_dipanggil,
        dataType: 'json',
        success: function(hasil) {
            var list = hasil["hasilBuku"];
            var obj_hasil = $("#hasil");
            obj_hasil.empty();

            for (i = 0; i < list.length; i++) {
                obj_hasil.append("<tr>")
                var judul_buku = list[i].judul; 
                var foto_buku = list[i].foto;
                var deskripsi_buku = list[i].deskripsi;
            
                obj_hasil.append("<td>" + "<img style='max-width: 128px' src=" + foto_buku +">" + "</td>");
                obj_hasil.append("<td><b>" + judul_buku + "</b><br>" + deskripsi_buku + "</td>");
                obj_hasil.append("</tr>")
            }
        }
    });
})