$(".toggle").click(function () { 
    $(this).parent().next().slideToggle();
  });
  
  $(".down").click(function () { 
    const select = $(this).parent().parent().parent()
    const selectClass = 'section-' + select.css('order')
    const nextClass = 'section-' + parseInt(parseInt(select.css('order')) + 1)  
  
    if(parseInt(select.css('order')) < 4){
      $("." + nextClass).addClass(selectClass)
      $("." + nextClass).removeClass(nextClass)
      select.removeClass(selectClass)
      select.addClass(nextClass)
    }
  });

  $(".up").click(function () { 
    const select = $(this).parent().parent().parent()
    const selectClass = 'section-' + select.css('order')
    const prevClass = 'section-' + parseInt(parseInt(select.css('order'))  - 1)
  
    if(parseInt(select.css('order')) > 1){
      $("." + prevClass).addClass(selectClass)
      $("." + prevClass).removeClass(prevClass)
      select.removeClass(selectClass)
      select.addClass(prevClass)
    }
  });