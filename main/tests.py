from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import Kegiatan, Peserta
from .form import FormKegiatan, FormPeserta, CreateUserForm
from .views import home, kegiatan, peserta


class MainTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.p = Peserta.objects.create(namaPeserta="Sultan",)
        self.home = reverse("main:home")
        self.kegiatan = reverse("main:kegiatan")
        self.peserta = reverse("main:peserta", args=[self.p.id])

    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/home.html")
    
    def test_GET_peserta(self):
        response = self.client.get(self.peserta)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/peserta.html")
    
    def test_GET_kegiatan(self):
        response = self.client.get(self.kegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/kegiatan.html")

    def test_POST_kegiatan(self):
        response = self.client.post(self.kegiatan,data= 
        {
            'nameKegiatan': 'PPW',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/kegiatan.html")   

class Authentication(TestCase):

    def test_register_page(self):
        response = self.client.get("/register/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/register.html')

        self.client.post('/register/', data={
            "username" : "sultan",
            "email" : "sultan@gmail.com",
            "password1" : "sultanta",
            "password2" : "sultanta"
        })

        response = self.client.get("/login/")
        self.assertEqual(response.status_code, 200)

    def test_login_page(self):
        response = self.client.get("/login/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/login.html') 
        
        self.client.post('/register/', data={
            "username" : "sultan",
            "email" : "sultan@gmail.com",
            "password1" : "sultanta",
            "password2" : "sultanta"
        })

        response = self.client.post("/login/", data={
            "username" : "dipo",
            "password" : "dipooo",
        })
        self.assertEqual(response.status_code, 200)

        self.client.post("/login/", data={
            "username" : "sultan",
            "password" : "sultanta",
        })
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        self.client.post('/register/', data={
            "username" : "sultan",
            "email" : "sultan@gmail.com",
            "password1" : "sultanta",
            "password2" : "sultanta"
        })
        
        self.client.post("/login/", data={
            "username" : "sultan",
            "password" : "sultanta",
        })

        self.client.get("/logout/")
        responseLogout = self.client.get("/login/")
        self.assertEqual(responseLogout.status_code, 200)
