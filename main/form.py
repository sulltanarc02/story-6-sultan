from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Kegiatan, Peserta

class FormKegiatan(forms.ModelForm) :
    class Meta:
        model = Kegiatan
        fields = [
            'namaKegiatan',
        ]

        widgets = {
            'namaKegiatan' : forms.TextInput(attrs={'class' : 'form-control'}),
        }

class FormPeserta(forms.ModelForm) :
    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
        ]

        widgets = {
            'namaPeserta' : forms.TextInput(attrs={'class' : 'form-control'}),
        }

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username',
                 'email', 
                 'password1', 
                 'password2'
        ]