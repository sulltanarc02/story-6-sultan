from django.db import models
from django.urls import reverse

# Create your models here.
class Peserta(models.Model):
    namaPeserta = models.CharField(max_length=100)
    class Meta:
        ordering = ['namaPeserta']


class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=100)
    peserta = models.ManyToManyField(Peserta)
    class Meta:
        ordering = ['namaKegiatan']

    def absolute_url(self):
        return reverse("main:peserta", kwargs = {"id" : self.id})
