from django.shortcuts import render, get_object_or_404, redirect
from .models import Kegiatan, Peserta
from .form import FormKegiatan, FormPeserta, CreateUserForm
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def home(request):
    tkegiatan = Kegiatan.objects.all()
    context = {
        "kegiatan" : tkegiatan
    }
    return render(request, 'main/home.html', context)

def kegiatan(request):
    form = FormKegiatan()

    if request.method == "POST":
        form = FormKegiatan(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")

    context = {
        "form" : form,
    }
    return render(request, 'main/kegiatan.html', context)

def peserta(request, *args, **kwargs):
    form = FormPeserta()

    if request.method == "POST":
        namaPeserta = request.POST.get("namaPeserta")
        pesertaDaftar = Peserta.objects.create(namaPeserta = namaPeserta)
        kegiatan = Kegiatan.objects.get(id = kwargs["id"])
        kegiatan.peserta.add(pesertaDaftar)
        return redirect("/")
    
    
    context = {
        "form" : form,
    }

    return render(request, "main/peserta.html", context)

def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.info(request, 'Username atau Password salah')
            return render(request, 'main/login.html')

    return render(request, 'main/login.html')

def logoutUser(request):
    logout(request)
    return redirect("/login/")

def registerPage(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Akun telah dibuat untuk ' + user)
            return redirect("/login/")

    context = {'form':form}
    return render(request, 'main/register.html', context)
